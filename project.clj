(defproject aoterrible "0.1.0-SNAPSHOT"
  :description "AOT Experiments for Clojure"
  :url "https://gitlab.com/semperos/aoterrible"
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :repl-options {:init-ns aoterrible.core}
  :profiles {:uberjar {:aot :all
                       :main aoterrible.core}})
