(ns aoterrible.core
  (:gen-class))

(defmacro defspecial
  [sym a-fn]
  (let [special-id (keyword "special" (name sym))
        special-name (str "special-" (name sym))]
    `(let [special-spec# {:id ~special-id}
           wrapped-fn# (fn [& args#]
                             (println "Special Time!")
                             (apply ~a-fn args#))]
       (def ~sym
         [special-spec# wrapped-fn#])
       (alter-meta! (var ~sym) merge {::special ~special-name}))))

(defspecial alpha (fn [& args] (map inc args)))
(defspecial beta (fn [& args] (filter odd? args)))

(defn collect-all-specials
  "Leverage all `defspecial`s to create a partial SystemMap of special
  components."
  []
  (let [special-name (comp ::special meta)]
    (into {} (comp (filter special-name)
                   (map #(vector (special-name %) (deref %))))
          (vals (ns-publics *ns*)))))

(def specials
  (collect-all-specials))

(defn -main
  "I don't do a whole lot."
  [& _args]
  (prn '*ns*-default *ns*) ;;=> clojure.core
  (prn 'compile-time-and-runtime-collected-meta-equal? (=  specials (collect-all-specials)));;=> false
  (binding [*ns* 'aoterrible.core]
    (prn '*ns*-explicit *ns*)
    (prn 'compile-time-and-runtime-collected-meta-equal? (=  specials (collect-all-specials))) ;;=> true
    ))
